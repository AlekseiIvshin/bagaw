﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnObstacleCollision();

public class Player : MonoBehaviour
{
    public OnObstacleCollision collisionDelegate;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacle" && collisionDelegate != null)
        {
            collisionDelegate();
        }
    }
}
