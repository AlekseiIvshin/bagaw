﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Setup : MonoBehaviour
{

    public GameObject player;
    public GameObject marker;
    public Button startButton;

    private GameObject playerGO;

    private List<GameObject> markers;

    private int cursor = 0;
    private Vector2 lastDirection;

    // Start is called before the first frame update
    void Start()
    {
        markers = new List<GameObject>();

        var startPosition = GameObject.Find("Start").transform;

        playerGO= GameObject.Instantiate(player, startPosition);
        playerGO.GetComponent<Player>().collisionDelegate = OnPlayerCollision;

        Button btn = startButton.GetComponent<Button>();
        btn.onClick.AddListener(StartRound);
    }

    // Update is called once per frame
    void Update()
    {
        //GameObject.Find("cat_a").GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, 0));
        //GameObject.Find("cat_b").GetComponent<Rigidbody2D>().AddForce(new Vector2(0.5f, 0));

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            mousePos.z = 0;

            markers.Add(GameObject.Instantiate(marker, mousePos, new Quaternion()));
        }

        //playerGO.GetComponent<Rigidbody2D>().AddForce(lastDirection);

        Debug.DrawLine(playerGO.transform.position, playerGO.transform.position + new Vector3(lastDirection.x, lastDirection.y, 0), Color.blue);
        Debug.DrawLine(playerGO.transform.position, playerGO.transform.position + new Vector3(playerGO.GetComponent<Rigidbody2D>().velocity.x, playerGO.GetComponent<Rigidbody2D>().velocity.y, 0), Color.red);

    }


    void StartRound()
    {
        cursor = 0;
        playerGO.GetComponent<Rigidbody2D>().gravityScale = 1;

        OnPlayerCollision();
    }

    void OnPlayerCollision()
    {
        //var distance = float.MaxValue;
        //var nextCursor = cursor;
        //var index = 0;
        //foreach(var marker in markers)
        //{
        //    if ((cursor ==0 ||index>=cursor) && (marker.transform.position - playerGO.transform.position).magnitude <= distance)
        //    {
        //        nextCursor = index;
        //        distance = (marker.transform.position - playerGO.transform.position).magnitude;
        //    }
        //    index++;
        //}

        //cursor = nextCursor;
        Debug.Log("Bums!");
        if (cursor >= markers.Count)
            return;


        Debug.Log("Next cursor is "+ cursor);

        var direction = Vector3.Normalize(markers[cursor].transform.position -playerGO.transform.position)*500f;

        playerGO.GetComponent<Rigidbody2D>().AddForce(-lastDirection);

        lastDirection = direction;

        playerGO.GetComponent<Rigidbody2D>().AddForce(lastDirection);

        Debug.Log(playerGO.GetComponent<Rigidbody2D>().velocity);

        cursor++;
    }
}
